package factory
import monedas.IMoneda

/**
 * Clase Factory
 */
abstract class Moneda_Factory {

    fun monedaCreada() {
        val moneda = crearMoneda()
        moneda.simbolo()
        moneda.codigo()
    }

    abstract fun crearMoneda(): IMoneda
}