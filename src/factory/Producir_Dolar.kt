package factory
import monedas.Dolar
import monedas.IMoneda

/**
 * Clase Producir Dolar
 */
class Producir_Dolar : Moneda_Factory() {
    override fun crearMoneda(): IMoneda {
        return Dolar()
    }
}