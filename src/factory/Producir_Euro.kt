package factory
import monedas.Euro
import monedas.IMoneda

/**
 * Clase Producir Euros
 */
class Producir_Euro : Moneda_Factory() {
    override fun crearMoneda(): IMoneda {
        return Euro()
    }
}