package monedas

/**
 * Implementacion de la moneda Euro.
 */
class Euro : IMoneda {
    override fun simbolo(){
        println("€")
    }
    override fun codigo(){
        println("EUR")
    }
}