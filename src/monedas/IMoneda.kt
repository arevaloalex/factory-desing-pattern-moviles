package monedas
/**
 * Interfaz comun para todas las monedas.
 */
interface IMoneda {
    fun simbolo()
    fun codigo()
}
