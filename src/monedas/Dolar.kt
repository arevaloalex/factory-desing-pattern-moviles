package monedas

/**
 * Implementacion de la moneda Dolar.
 */
class Dolar : IMoneda {
    override fun simbolo(){
        println("$")
    }
    override fun codigo(){
        println("USD")
    }
}